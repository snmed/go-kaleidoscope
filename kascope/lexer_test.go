package kascope

import (
	"bufio"
	"bytes"
	"testing"
)

func TestGetTok(t *testing.T) {
	// Arrange
	data := []byte(`# Compute the x'th fibonacci number.
def fib(x)
  if x < 3 then
    1
  else
    fib(x-1)+fib(x-2)

# This expression will compute the 40th number.
fib(40)`)

	tokens := [...]token{
		token{typ: -2, value: nil},
		token{typ: -4, value: "fib"},
		token{typ: -6, value: '('},
		token{typ: -4, value: "x"},
		token{typ: -6, value: ')'},
		token{typ: -4, value: "if"},
		token{typ: -4, value: "x"},
		token{typ: -6, value: '<'},
		token{typ: -5, value: 3.},
		token{typ: -4, value: "then"},
		token{typ: -5, value: 1.},
		token{typ: -4, value: "else"},
		token{typ: -4, value: "fib"},
		token{typ: -6, value: '('},
		token{typ: -4, value: "x"},
		token{typ: -6, value: '-'},
		token{typ: -5, value: 1.},
		token{typ: -6, value: ')'},
		token{typ: -6, value: '+'},
		token{typ: -4, value: "fib"},
		token{typ: -6, value: '('},
		token{typ: -4, value: "x"},
		token{typ: -6, value: '-'},
		token{typ: -5, value: 2.},
		token{typ: -6, value: ')'},
		token{typ: -4, value: "fib"},
		token{typ: -6, value: '('},
		token{typ: -5, value: 40.},
		token{typ: -6, value: ')'},
		token{typ: -1, value: nil},
	}

	reader := bufio.NewReader(bytes.NewReader(data))
	lexer := newLexer(reader)

	// Act & Assert
	current := 0
	var tok token
	for tok = lexer.getTok(); tok.typ != tokEOF; tok = lexer.getTok() {
		expected := tokens[current]
		if expected.typ != tok.typ || expected.value != tok.value {
			t.Fatalf("Current: %v; Expected typ: %v -> Actual: %v; Expected value: %v -> Actual: %v",
				current, expected.typ, tok.typ, expected.value, tok.value)
		}
		current++
	}
	if current > len(tokens) {
		t.Fatalf("Count mismatch, Expected tokens: %v, Actual: %v", len(tokens), current)
	}

	if tok.typ != tokEOF {
		t.Fatalf("Last token is %v instead of %v", tok.typ, tokEOF)
	}

}
