package kascope

import (
	"io"
	"strconv"
	"unicode"
)

type tokenTyp int

const (
	tokEOF tokenTyp = -1
	// Commands
	tokDef    tokenTyp = -2
	tokExtern tokenTyp = -3
	// Primary
	tokIdentifier tokenTyp = -4
	tokNumber     tokenTyp = -5
	tokRune       tokenTyp = -6
)

type token struct {
	typ   tokenTyp
	value interface{}
}

type lexer interface {
	getTok() token
}

type lex struct {
	io.RuneScanner
}

func newLexer(r io.RuneScanner) lexer {
	return &lex{RuneScanner: r}
}

func mustOk(err error) {
	if err != nil {
		panic(err)
	}
}

func (l *lex) getTok() token {
	lastChar := rune(' ')
	var err error
	// Skip any whitespace
	for unicode.IsSpace(lastChar) {
		lastChar, _, err = l.ReadRune()
		if err == io.EOF {
			return token{typ: tokEOF, value: nil}
		}
		mustOk(err)
	}
	// Check if alpha numeric
	if unicode.IsLetter(lastChar) {
		ident := string(lastChar)
		for lastChar, _, err = l.ReadRune(); unicode.IsLetter(lastChar) || unicode.IsNumber(lastChar); lastChar, _, err = l.ReadRune() {
			ident += string(lastChar)
		}
		l.UnreadRune()

		if ident == "def" {
			return token{typ: tokDef, value: nil}
		}
		if ident == "extern" {
			return token{typ: tokExtern, value: nil}
		}
		return token{typ: tokIdentifier, value: ident}
	}

	if isDigit(lastChar) {
		ident := string(lastChar)
		for lastChar, _, err = l.ReadRune(); isDigit(lastChar); lastChar, _, err = l.ReadRune() {
			ident += string(lastChar)
		}
		l.UnreadRune()

		number, err := strconv.ParseFloat(ident, 64)
		mustOk(err)
		return token{typ: tokNumber, value: number}
	}

	if lastChar == '#' {
		// Eat comment until end of line
		for lastChar, _, err = l.ReadRune(); lastChar != '\n' && lastChar != '\r' && err != io.EOF; lastChar, _, err = l.ReadRune() {
			if err == io.EOF {
				return token{typ: tokEOF, value: nil}
			}
			mustOk(err)
		}

		return l.getTok()
	}

	return token{typ: tokRune, value: lastChar}
}

func isDigit(r rune) bool {
	return unicode.IsNumber(r) || r == '.'
}
