# go-kaleidoscope #

This is a implementation of the [LLVM Kaleidoscope Tutorial](http://llvm.org/docs/tutorial/LangImpl01.html) in go. Every end of a chapter is tagged and serves as starting point for the next chapter.

The purpose of this repo is to get familiar with the go programming language.